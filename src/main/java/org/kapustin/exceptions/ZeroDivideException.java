package org.kapustin.exceptions;

public class ZeroDivideException extends RuntimeException {

    public ZeroDivideException(String message) {
        super(message);
    }

}
