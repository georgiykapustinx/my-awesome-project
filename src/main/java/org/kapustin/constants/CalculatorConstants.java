package org.kapustin.constants;

public class CalculatorConstants {
    public static final String OPERATION_RESULT = "Результат операции: ";
    public static final String INPUT_NUMBER = "Введите число: ";
    public static final String ERROR_NUMBER = "Ошибка при вводе числа";
    public static final String CHOOSE_OPERATION = "Выберите действие: ";
    public static final String ERROR_OPERATION = "Неверная операция";
    public static final String SUM_ERROR = "Оба числа равны нулю или отрицательные числа";
    public static final String DIFF_ERROR = "Результат - отрицательное число";
    public static final String MULT_ERROR = "Одно или оба числа равны нулю";
    public static final String DIV_ZERO_ERROR = "Деление на ноль запрещено";
    public static final char SYMBOL_PLUS = '+';
    public static final char SYMBOL_MINUS = '-';
    public static final char SYMBOL_MULTIPLY = '*';
    public static final char SYMBOL_DIVIDE = '/';
}
