package org.kapustin.service.implementation;

import org.kapustin.constants.CalculatorConstants;
import org.kapustin.dto.CalculatorHolderDto;
import org.kapustin.exceptions.ZeroDivideException;
import org.kapustin.service.intrface.CalculatorService;
import org.kapustin.service.intrface.InputService;


public class CalculatorServiceImpl implements CalculatorService {
    private static final InputService Input = new InputServiceImpl(); //Импортирую из InputService

    @Override
    public int summary(int num1, int num2) {
        int sum = num1 + num2;
        if (sum < 0) {
            System.out.println(CalculatorConstants.SUM_ERROR);
        } else {
            return sum;
        }
        return sum;
    }

    @Override
    public int diff(int num1, int num2) {
        int diff = num1 - num2;
        if (diff < 0) {
            System.out.println(CalculatorConstants.DIFF_ERROR);
        } else {
            return diff;
        }
        return diff;
    }

    @Override
    public int multiply(int num1, int num2) {
        int mult = num1 * num2;
        if (mult < 0) {
            System.out.println(CalculatorConstants.MULT_ERROR);
        } else {
            return mult;
        }
        return mult;
    }

    @Override
    public int divide(int num1, int num2) throws ZeroDivideException {
        try {
            return num1 / num2;
        } catch (ArithmeticException exception) {
            throw new ZeroDivideException(CalculatorConstants.DIV_ZERO_ERROR);
        }
    }

    @Override
    public int calculateOperation(CalculatorHolderDto calculatorHolderDto) throws ZeroDivideException {
        int num1 = calculatorHolderDto.getNum1();
        int num2 = calculatorHolderDto.getNum2();
        char operation = calculatorHolderDto.getOperation();
        return switch (operation) {
            case CalculatorConstants.SYMBOL_PLUS -> summary(num1, num2);
            case CalculatorConstants.SYMBOL_MINUS -> diff(num1, num2);
            case CalculatorConstants.SYMBOL_MULTIPLY -> multiply(num1, num2);
            case CalculatorConstants.SYMBOL_DIVIDE -> divide(num1, num2);
            default -> calculateOperation(calculatorHolderDto);
        };
    }

    @Override
    public int calculatorService() throws ZeroDivideException {
        CalculatorHolderDto calculatorHolderDto = Input.getCalculatorHolderDto();
        int result = calculateOperation(calculatorHolderDto);
        System.out.println(CalculatorConstants.OPERATION_RESULT + result);
        return result;
    }
}


