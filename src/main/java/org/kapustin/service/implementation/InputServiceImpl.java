package org.kapustin.service.implementation;

import org.kapustin.constants.CalculatorConstants;
import org.kapustin.dto.CalculatorHolderDto;
import org.kapustin.service.intrface.InputService;

import java.util.Scanner;

public class InputServiceImpl implements InputService {
    Scanner sc = new Scanner(System.in);

    @Override
    public CalculatorHolderDto getCalculatorHolderDto() {
        int num1 = getInt();
        char operation = getOperation();
        int num2 = getInt();
        return new CalculatorHolderDto(num1, num2, operation);
    }

    private int getInt() {
        System.out.println(CalculatorConstants.INPUT_NUMBER);
        int num;
        if (sc.hasNextInt()) {
            num = sc.nextInt();
        } else {
            System.out.println(CalculatorConstants.ERROR_NUMBER);
            sc.next();
            num = getInt();
        }
        return num;
    }

    private char getOperation() {
        System.out.println(CalculatorConstants.CHOOSE_OPERATION);
        char operation;
        if (sc.hasNext()) {
            operation = sc.next().charAt(0);
        } else {
            System.out.println(CalculatorConstants.ERROR_OPERATION);
            sc.next();
            operation = getOperation();
        }
        return operation;
    }


}
